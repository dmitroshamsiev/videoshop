﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for Image.xaml
    /// </summary>
    public partial class ImageWindow : Window
    {

        public ImageWindow( Movie movie )
        {
            InitializeComponent();
            this.MaxHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            DataContext = movie;
        }

        private void MovieImage_MouseDown( object sender, MouseButtonEventArgs e )
        {
            this.Close();
        }
    }
}
