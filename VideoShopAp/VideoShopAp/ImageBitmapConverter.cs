﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace VideoShopAp
{
    public class ImageBitmapConverter
        : IValueConverter
    {

        static Uri _default = "..\\..\\default.jpg".CreateFullPath();

        #region IValueConverter Members

        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var path = value as string;
            return new BitmapImage( string.IsNullOrWhiteSpace( path ) ? _default : path.CreateFullPath() );
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
