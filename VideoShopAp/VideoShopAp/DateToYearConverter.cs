﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace VideoShopAp
{
    class DateToYearConverter
        : IValueConverter
    {
        #region IValueConverter Members

        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var date = (DateTime)value;
            return date.Year.ToString();
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            DateTime date;
            DateTime.TryParseExact( value.ToString(), "yyyy", culture, System.Globalization.DateTimeStyles.None, out date );
            return date;
        }

        #endregion
    }
}
