﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VideoShopAp.Controls
{
    /// <summary>
    /// Interaction logic for MovieIcon.xaml
    /// </summary>
    public partial class MovieIcon : UserControl
    {
        public static readonly DependencyProperty IconSourceProperty = 
            DependencyProperty.Register(
            "IconSource",
            typeof( ImageSource ),
            typeof( MovieIcon ),
            new PropertyMetadata(default(ImageSource)) );

        public ImageSource IconSource 
        {
            get
            {
                return GetValue( IconSourceProperty ) as ImageSource;
            }
            set
            {
                SetValue( IconSourceProperty, value );
            }
        }

       
        public MovieIcon()
        {
            InitializeComponent();
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            var movie = Tag as Movie;
            var win = new ImageWindow( movie );
            win.ShowDialog();
        }
    }
}
