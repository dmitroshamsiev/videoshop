﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditFormat.xaml
    /// </summary>
    public partial class EditFormat : Window
    {
        /// <summary>
        /// Format to edit
        /// </summary>
        public Format Format { get; private set; }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public EditFormat(Format format)
        {
            InitializeComponent();
            Format = format;
            DataContext = this;
        }
        /// <summary>
        /// Saves format and closes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveFormat( object sender, RoutedEventArgs e )
        {
            this.Close();
        }
    }
}
