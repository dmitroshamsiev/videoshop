﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace VideoShopAp
{
    public class SalesReportConverter
        : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var sales = value as ListCollectionView;
            sales.Refresh();
            decimal total = default(Decimal);
            decimal purchaseTotal = default(Decimal);
            foreach (Sale s in sales)
            {
                var salePrice= s.Quantity * s.Movie.SalesPrice;
                if (s.Customer != null && s.Customer.Discont != null)
                    salePrice = salePrice - salePrice * s.Customer.Discont.Percent / 100;
                total += salePrice;
                purchaseTotal += s.Quantity * s.Movie.PurchasePrice;
            }
            return String.Format(culture, "Всего : {0:C}, Чистая прибыль : {1:C}", total, total - purchaseTotal);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
