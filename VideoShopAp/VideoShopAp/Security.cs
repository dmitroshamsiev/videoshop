﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace VideoShopAp
{
    public class Security : INotifyPropertyChanged
    {
        /// <summary>
        /// Video shop data context
        /// </summary>
        VideoShopData _context;
        // <summary>
        /// Indicates, whether the current user is administrator.
        /// </summary>
        bool m_isAdmin;
        /// <summary>
        /// Current logged in user.
        /// </summary>
        Membership m_currentUser;
        /// <summary>
        /// Users name with role.
        /// </summary>
        string m_name;

        /// <summary>
        /// Current logged in user.
        /// </summary>
        public Membership CurrentUser
        {
            get { return m_currentUser; }
            set
            {
                m_currentUser = value;
                IsAdministrator = null== CurrentUser
                    ? false
                    : CurrentUser.UserInRoles.FirstOrDefault( ur => ur.RoleId == 1 ) != null;

                Name = String.Format( "{0} - {1}", m_currentUser.Employee.Name, m_currentUser.UserInRoles.First().Role.Name );
            }

        }

        /// <summary>
        /// User's name with role
        /// </summary>
        public string Name 
        {
            get { return m_name; }
            set
            {
                m_name = value;
                OnPropertyChanged( "Name" );
            }
        }

        /// <summary>
        /// Indicates, whether the current user is administrator.
        /// </summary>
        public bool IsAdministrator
        {
            get
            {
                return m_isAdmin;
            }
            set
            {
                m_isAdmin = value;
                OnPropertyChanged( "IsAdministrator" );
            }
        }
        /// <summary>
        /// Initializes a new instance of the Security class.
        /// </summary>
        /// <param name="context">Video shop data context.</param>
        public Security( VideoShopData context )
        {
            _context = context;
        }
        /// <summary>
        /// Creates a new user for specified employee.
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="password">Password</param>
        /// <param name="employeeId">Employee ID</param>
        /// <param name="roleId">Role ID</param>
        /// <returns><b>true</b> on success, otherwise - <b>false</b>.</returns>
        public void CreateUser( string userName, string password, int employeeId, int roleId )
        {
            // create password salt.
            string salt = CreatePasswordSalt();

            // make password "salted"
            string saltedPassword = CreateHashedPassword( password, salt );

            // create membership
            var membership = _context.Memberships.Add( new Membership
            {
                UserName = employeeId,
                EmployeeId = employeeId,
                Password = saltedPassword,
                PasswordSalt = salt
            } );

            _context.SaveChanges();

            // set specified role
            _context.UserInRoles.Add( new UserInRole { MembershipId = membership.Id, RoleId = roleId } );

            // save changes
            _context.SaveChanges();

            // set current user.
            //CurrentUser = membership;
        }
        /// <summary>
        /// Logs the user in.
        /// </summary>
        /// <param name="id">User id</param>
        /// <param name="password">User password</param>
        /// <returns><b>true</b> on success, otherwise - <b>false</b>.</returns>
        public bool Login( int id, string password )
        {
            // get user with specified ID
            var user = _context.Memberships.FirstOrDefault( m => m.EmployeeId == id );
            if ( null == user )
                throw new Exception( "Для выбранного вами сотрудника не существует учетной записи." );


            var res =  PasswordIsRight( password, user.Password, user.PasswordSalt );
            if ( !res )
                throw new Exception( "Неверный пароль. Повторите попытку." );

            CurrentUser = user;
            return true;
        }

        /// <summary>
        /// Creates md5 hash string
        /// </summary>
        /// <param name="input">Source string</param>
        /// <returns>md5 hash string</returns>
        string GetMd5Hash( string input )
        {
            byte[] data;
            using ( MD5 md5 = MD5.Create() )
            {
                // Convert the input string to a byte array and compute the hash.
                data = md5.ComputeHash( Encoding.UTF8.GetBytes( input ) );
            }
            return Convert.ToBase64String( data );
        }
        /// <summary>
        /// Creates md5 hashed and "salted" password
        /// </summary>
        /// <param name="password">Password to get hashed and salted</param>
        /// <param name="salt">Salt</param>
        /// <returns>Md5 hashed and "salted" password</returns>
        string CreateHashedPassword( string password, string salt )
        {
            string hash = GetMd5Hash( password );
            return GetMd5Hash( hash + salt );
        }
        /// <summary>
        /// Creates password salt.
        /// </summary>
        /// <returns>Password salt.</returns>
        string CreatePasswordSalt()
        {
            return CreateRandomString();
        }
        /// <summary>
        /// Creates random string
        /// </summary>
        /// <returns>Random string</returns>
        string CreateRandomString()
        {
            return Regex.Replace( Guid.NewGuid().ToString(), "-", "" );
        }
        /// <summary>
        /// Checks whether two passwords are identical base.
        /// </summary>
        /// <param name="password1">Original pasword entered by user (not hashed)</param>
        /// <param name="password2">Second password that is alreadey hashed.</param>
        /// <param name="salt">Password salt.</param>
        /// <returns><b>True</b> if password is right, otherwise - <b>false</b>.</returns>
        bool PasswordIsRight( string password1, string password2, string salt )
        {
            // entered password hash
            string eph = CreateHashedPassword( password1, salt );
            return CompareMD5Strings( eph, password2 );
        }
        /// <summary>
        /// Compares to MD5 hashed strings.
        /// </summary>
        /// <param name="hash1">First MD5 hashed string.</param>
        /// <param name="hash2">Second MD5 hashed string.</param>
        /// <returns><b>true</b> if both MD5 hashed strings have one source, otherwise - <b>false</b>.</returns>
        bool CompareMD5Strings( string hash1, string hash2 )
        {
            byte[] bytes1 = Convert.FromBase64String( hash1 );
            byte[] bytes2 = Convert.FromBase64String( hash2 );

            // same reference or both are null values.
            if ( bytes1 == bytes2 ) return true;
            // check for either one of the values for null or by size.
            if ( null == bytes1 || bytes2 == null || bytes1.Length != bytes2.Length ) return false;

            // non-null, same size hash sequences. Check individual bytes.
            for ( int i = 0; i < bytes1.Length; ++i )
            {
                // find the first mismatching byte.
                if ( bytes1[ i ] != bytes2[ i ] ) return false;
            }
            // same hash values.
            return true;
        }

        /// <summary>
        /// Arisen when a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Arises <see cref="PropertyChanged"/>
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void OnPropertyChanged( string propertyName )
        {
            if ( null != PropertyChanged ) PropertyChanged( this, new PropertyChangedEventArgs( propertyName ) );
        }
    }
}
