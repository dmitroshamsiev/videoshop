﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace VideoShopAp
{
    public class BoolToVisibilityConverter
        : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var boolValue = ( bool )value;
            if ( boolValue )
                return Visibility.Visible;
            return Visibility.Hidden;
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var visibilityValue = ( Visibility )value;
            if ( visibilityValue == Visibility.Hidden )
                return false;
            return true;
        }
    }
}
