﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace VideoShopAp
{
    public class TotalPriceConvertor
        :IValueConverter
    {
        #region IValueConverter Members

        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var sale = value as Sale;
            var total = sale.Movie.SalesPrice * sale.Quantity;
            if ( sale.Customer != null && sale.Customer.Discont != null )
                total = total - ( total * sale.Customer.Discont.Percent / 100 );
            return String.Format( culture, "{0:C}", total );
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
