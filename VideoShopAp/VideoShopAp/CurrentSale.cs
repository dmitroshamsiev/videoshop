﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace VideoShopAp
{
    public class CurrentSale
          : ObservableCollection<Sale>
    {
        Customer m_customer;

        public Employee CurrentEmployee { get; set; }
        public Customer CurrentCustomer
        {
            get { return m_customer; }
            set { m_customer = value; OnPropertyChanged(new PropertyChangedEventArgs("CurrentCustomer")); }
        }
        public decimal TotalPrice
        {
            get
            {
                if ( this.Count == 0 )
                    return 0;
                return this
                    .Sum( s => { return s.Quantity * s.Movie.SalesPrice; } );
            }
        }

        public void Refresh()
        {
            OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ) );
        }

        public void Add( Movie movie )
        {
            var sale = this.FirstOrDefault( s => s.Movie.Id == movie.Id );
            if ( null != sale )
            {
                sale.Quantity += 1;
                OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ) );
            }
            else
            {
                this.Add( new Sale
                {
                    Employee = CurrentEmployee,
                    Customer = CurrentCustomer,
                    Movie = movie,
                    Date = DateTime.Now,
                    Quantity = 1
                } );
            }
        }

        protected override void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            OnPropertyChanged( new PropertyChangedEventArgs( "TotalPrice" ) );
            base.OnCollectionChanged( e );
        }
    }
}
