﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace VideoShopAp
{
    public static class Extensions
    {
        /// <summary>
        /// Gets the program's (primary assembly) root folder.
        /// </summary>
        /// <returns>The program's (primary assembly) root folder.</returns>
        public static string GetProgramRoot()
        {
            var assambly = Assembly.GetEntryAssembly();
            if ( assambly == null )
            {
                assambly = Assembly.GetExecutingAssembly();
                if ( assambly == null )
                    throw new NullReferenceException( "Can't get entry assambly. It's null." );
            }
            var value = Path.GetDirectoryName( assambly.Location );
            //Trace.WriteLine( "GetProgramRoot " + value );
            return value;
        }

        /// <summary>
        /// Gets full path for specified path that can be absolute or relative
        /// </summary>
        /// <param name="path">absolute or relative path</param>
        /// <returns></returns>
        public static string GetFullPath( this string path )
        {
            //Trace.WriteLine( "Get full path for " + path );
            var value = Path.IsPathRooted( path )
                ? path
                : Path.Combine( GetProgramRoot(), path );
            //Trace.WriteLine( "GetFullPath " + value );
            return value;
        }

        public static Uri CreateFullPath( this string path )
        {
            return new Uri( path.GetFullPath() );
        }

        /// <summary>
        /// Retrieves a substring from specified string.
        /// </summary>
        /// <param name="value">String to get substring from.</param>
        /// <param name="length">The number of characters in the substring.</param>
        /// <param name="throwEx">Indicates whether to throw exception if specified length of substring is greater than value's length.</param>
        /// <returns>Substring</returns>
        public static string Substring( this string value, int length, bool throwEx )
        {
            if ( throwEx && length < value.Length )
                throw new ArgumentException( String.Format( "{0}'s length is greater than {1}", length, value ) );

            if ( value.Length < length )
                return value;

            return value.Substring( 0, length );
        }
    }
}
