﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        /// <summary>
        /// Video shop data context. Test comment
        /// </summary>
        private VideoShopData _context;
        /// <summary>
        ///  Security helper.
        /// </summary>
        public Security Security { get; set; }

        public CurrentSale CurrentSale { get; set; }
        public ListCollectionView FilteredMovies { get; set; }
        public ListCollectionView FilteredClients { get; set; }
        public ListCollectionView GenreFilter { get; set; }
        public ListCollectionView CategoryFilter { get; set; }
        public ListCollectionView FormatFilter { get; set; }
        public ListCollectionView SalesReport { get; set; }

        public Genre SelectedGenreFilter { get; set; }
        public Category SelectedCategoryFilter { get; set; }
        public Format SelectedFormatFilter { get; set; }

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Instantiate the data context
            _context = new VideoShopData();
            Security = new Security( _context );
            CurrentSale = new CurrentSale();
            _context.Movies.Load();
            FilteredMovies = new ListCollectionView( _context.Movies.Local );

            _context.Genres.Load();
            GenreFilter = new ListCollectionView( _context.Genres.Local );

            _context.Categories.Load();
            CategoryFilter = new ListCollectionView( _context.Categories.Local );

            _context.Formats.Load();
            FormatFilter = new ListCollectionView( _context.Formats.Local );

            _context.Customers.Load();
            FilteredClients = new ListCollectionView( _context.Customers.Local );

            BindFilters();

            DateFrom.SelectedDate = DateTime.Today.AddMonths( -1 );
            DateTo.SelectedDate = DateTime.Now;

            _context.Sales.Load();
            SalesReport = new ListCollectionView( _context.Sales.Local );


            DataContext = this;
        }
        /// <summary>
        /// Handles <see cref="Loaded"/> event of the Window.
        /// </summary>
        /// <param name="sender">MainWindow instance</param>
        /// <param name="e">Event arguments.</param>
        private void MainWindowLoadedHandler( object sender, RoutedEventArgs e )
        {
            // admin existed if there are at least one employee who has "Administrator" role.
            var adminExists = null != _context.UserInRoles.FirstOrDefault( ur => ur.RoleId == 1 );

            FirstTimeWindow.Visibility = adminExists
                ? Visibility.Collapsed
                : Visibility.Visible
                ;

            _context.Employees.Load();

            var view = new ListCollectionView( _context.Employees.Local );
            Locker.DataContext = view;
        }
        /// <summary>
        /// Handles <see cref="Unloaded"/> event of the Window
        /// </summary>
        /// <param name="sender">MainWindow instance</param>
        /// <param name="e">Event arguments.</param>
        private void MainWindowUnloadedHandler( object sender, RoutedEventArgs e )
        {
            if ( null == _context )
                return;

            _context.Dispose();
            _context = null;
        }


        private void SaveChanges( object sender, RoutedEventArgs e )
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// Creates a new membership account for administrator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateAdminButtonClick( object sender, RoutedEventArgs e )
        {
            // get selected employee
            var selectedEmployee = EmployeesForAdminRole.SelectedItem as Employee;

            // create new membership account  with "Administrator" role
            Security.CreateUser( selectedEmployee.Name, AdminPassword.Password, selectedEmployee.Id, 1 );

            if ( null != Security.CurrentUser )
            {
                FirstTimeWindow.Visibility = System.Windows.Visibility.Collapsed;
                Locker.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        /// <summary>
        ///  Show Warning message.
        /// </summary>
        /// <param name="message">Message text</param>
        void WarningMessage( string message )
        {
            MessageBox.Show( message, "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning );
        }

        /// <summary>
        /// Checks employee that wants to login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginButtonClick( object sender, RoutedEventArgs e )
        {
            // get selected user;
            var user = LoginEmployees.SelectedItem as Employee;
            try
            {
                if ( Security.Login( user.Id, LoginPassword.Password ) )
                {
                    Locker.Visibility = System.Windows.Visibility.Collapsed;
                    CurrentSale.CurrentEmployee = Security.CurrentUser.Employee;
                    return;
                }
            }
            catch ( Exception ex )
            {
                LoginError.Text = ex.Message;
            }
        }
        /// <summary>
        /// Handles tab changing under EDIT tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditTabChanged( object sender, SelectionChangedEventArgs e )
        {
            if ( CategoriesTab.IsSelected )
            {
                // bind categories
                BindCategories();
                return;
            }

            if ( FormatsTab.IsSelected )
            {
                // bind formats
                BindFormats();
                return;
            }

            if ( GenresTab.IsSelected )
            {
                // bind genres
                BindGenres();
                return;
            }

            if ( MoviesTab.IsSelected )
            {
                // bind movies
                BindMovies();
                return;
            }

            if ( DiscontTab.IsSelected )
            {
                BindDiscont();
                return;
            }
            if ( CustomersTab.IsSelected )
            {
                BindCustomers();
                return;
            }
            if ( PositionsTab.IsSelected )
            {
                BindPositions();
                return;
            }
            if ( EmployeesTab.IsSelected )
            {
                BindEmployees();
                return;
            }
            if ( AccountsTab.IsSelected )
                BindAccounts();
        }

        void BindAccounts()
        {
            _context.Memberships.Load();

            var view = new ListCollectionView( _context.Memberships.Local );
            AccountsTab.DataContext = view;
        }

        void BindEmployees()
        {
            _context.Employees.Load();

            var view = new ListCollectionView( _context.Employees.Local );
            EmployeesTab.DataContext = view;
        }

        void BindPositions()
        {
            _context.Positions.Load();

            var view = new ListCollectionView( _context.Positions.Local );
            PositionsTab.DataContext = view;
        }

        void BindCustomers()
        {
            _context.Customers.Load();

            var view = new ListCollectionView( _context.Customers.Local );
            CustomersTab.DataContext = view;
        }

        /// <summary>
        /// Binds disconts to its listview
        /// </summary>
        private void BindDiscont()
        {
            _context.Disconts.Load();

            var view = new ListCollectionView( _context.Disconts.Local );
            DiscontTab.DataContext = view;
        }

        /// <summary>
        /// Binds categories to EditCategoriesListView
        /// </summary>
        private void BindCategories()
        {
            _context.Categories.Load();

            var view = new ListCollectionView( _context.Categories.Local );
            CategoriesTab.DataContext = view;
        }
        /// <summary>
        /// Binds formats to EditFormatsListView
        /// </summary>
        private void BindFormats()
        {
            _context.Formats.Load();
            var view = new ListCollectionView( _context.Formats.Local );
            FormatsTab.DataContext = view;
        }

        /// <summary>
        /// Binds formats to EditGenresListView
        /// </summary>
        private void BindGenres()
        {
            _context.Genres.Load();
            var view = new ListCollectionView( _context.Genres.Local );
            GenresTab.DataContext = view;
        }

        /// <summary>
        /// Bind Movies to MoviesTab
        /// </summary>
        void BindMovies()
        {
            _context.Movies.Load();
            var view = new ListCollectionView( _context.Movies.Local );
            MoviesTab.DataContext = view;
        }
        /// <summary>
        /// Deletes categories from database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteCategory( object sender, RoutedEventArgs e )
        {
            var category = GetButtonTag<Category>( sender );
            _context.Categories.Remove( category );
            _context.SaveChanges();
        }
        /// <summary>
        /// Opens a new window that allows to edit a category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditCategory( object sender, RoutedEventArgs e )
        {
            var category = GetButtonTag<Category>( sender );
            var editCategoryWindow = new EditCategory( category );

            editCategoryWindow.Closed += ( s, args ) =>
                {
                    _context.SaveChanges();
                };
            editCategoryWindow.ShowDialog();

        }
        /// <summary>
        /// Opens a window to add new category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewCategory( object sender, RoutedEventArgs e )
        {
            var newCategoryWindow = new EditCategory( new Category() );
            newCategoryWindow.Closed += ( s, args ) =>
                {
                    _context.Categories.Add( newCategoryWindow.Category );
                    _context.SaveChanges();
                };
            newCategoryWindow.ShowDialog();
        }
        /// <summary>
        /// Deletes format from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteFormat( object sender, RoutedEventArgs e )
        {
            var format = GetButtonTag<Format>( sender );
            _context.Formats.Remove( format );
            _context.SaveChanges();
        }
        /// <summary>
        /// Opens a new window to edit format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditFormat( object sender, RoutedEventArgs e )
        {
            var format = GetButtonTag<Format>( sender );
            var window = new EditFormat( format );
            window.Closed += ( s, a ) => _context.SaveChanges();
            window.ShowDialog();
        }
        /// <summary>
        /// Opens a new window to add format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewFormat( object sender, RoutedEventArgs e )
        {
            var window = new EditFormat( new Format() );
            window.Closed += ( s, a ) =>
                {
                    _context.Formats.Add( window.Format );
                    _context.SaveChanges();
                };
            window.ShowDialog();
        }

        /// <summary>
        /// Gets an instance from button's tag
        /// </summary>
        /// <param name="button">Button</param>
        /// <returns>Instance type of T</returns>
        private T GetButtonTag<T>( object button )
            where T : class
        {
            var btn = button as Button;
            return btn.Tag as T;
        }

        /// <summary>
        ///  Deletes genre from database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteGenre( object sender, RoutedEventArgs e )
        {
            var genre = GetButtonTag<Genre>( sender );
            _context.Genres.Remove( genre );
            _context.SaveChanges();
        }
        /// <summary>
        /// Edited selected genre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditGenre( object sender, RoutedEventArgs e )
        {
            var genre = GetButtonTag<Genre>( sender );
            var window = new EditGenre( genre );
            window.Closed += ( s, a ) => _context.SaveChanges();
            window.ShowDialog();
        }
        /// <summary>
        /// Adds new genre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewGenre( object sender, RoutedEventArgs e )
        {
            var window = new EditGenre( new Genre() );
            window.Closed += ( s, a ) =>
            {
                _context.Genres.Add( window.Genre );
                _context.SaveChanges();
            };
            window.ShowDialog();
        }

        private void DeleteMovie( object sender, RoutedEventArgs e )
        {

        }
        /// <summary>
        ///  Allows to edit a movie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditMovie( object sender, RoutedEventArgs e )
        {
            var movie = GetButtonTag<Movie>( sender );

            _context.Formats.Load();
            _context.Categories.Load();
            _context.Genres.Load();

            var window = new EditMovie(
                movie,
                new ListCollectionView( _context.Categories.Local ),
                new ListCollectionView( _context.Genres.Local ),
                new ListCollectionView( _context.Formats.Local ) );
            window.Closed += ( s, a ) => _context.SaveChanges();
            window.ShowDialog();
        }

        private void NewMovie( object sender, RoutedEventArgs e )
        {
            _context.Formats.Load();
            _context.Categories.Load();
            _context.Genres.Load();

            var window = new EditMovie(
                new Movie(),
                new ListCollectionView( _context.Categories.Local ),
                new ListCollectionView( _context.Genres.Local ),
                new ListCollectionView( _context.Formats.Local ) );
            window.Closed += ( s, a ) =>
            {
                try
                {
                    _context.Movies.Add( window.Movie );
                    _context.SaveChanges();
                }
                catch ( Exception ex )
                {
                    _context.Movies.Remove( window.Movie );
                }
            };
            window.ShowDialog();
        }

        private void DeleteDiscont( object sender, RoutedEventArgs e )
        {
            var discont = GetButtonTag<Discont>( sender );
            _context.Disconts.Remove( discont );
            _context.SaveChanges();
        }

        private void EditDiscont( object sender, RoutedEventArgs e )
        {
            var discont = GetButtonTag<Discont>( sender );
            var window = new EditDiscont( discont );
            window.Closed += ( s, a ) => _context.SaveChanges();
            window.ShowDialog();
        }
        /// <summary>
        /// Allows to create a new Discont instance.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewDiscont( object sender, RoutedEventArgs e )
        {
            var window = new EditDiscont( new Discont() );
            window.Closed += ( s, a ) =>
                {
                    _context.Disconts.Add( window.Discont );
                    _context.SaveChanges();
                };
            window.ShowDialog();
        }

        private void DeleteCustomer( object sender, RoutedEventArgs e )
        {
            var customer = GetButtonTag<Customer>( sender );
            _context.Customers.Remove( customer );
            _context.SaveChanges();
        }

        private void EditCustomer( object sender, RoutedEventArgs e )
        {
            var customer = GetButtonTag<Customer>( sender );
            var window = new EditCustomer( customer );
            window.Closed += ( s, a ) => _context.SaveChanges();
            window.ShowDialog();
        }

        private void NewCustomer( object sender, RoutedEventArgs e )
        {
            var window = new EditCustomer( new Customer() );
            window.Closed += ( s, a ) =>
                {
                    _context.Customers.Add( window.Customer );
                    _context.SaveChanges();
                };
            window.ShowDialog();
        }

        private void DeletePosition( object sender, RoutedEventArgs e )
        {
            var position = GetButtonTag<Position>( sender );
            _context.Positions.Remove( position );
            _context.SaveChanges();
        }

        private void EditPosition( object sender, RoutedEventArgs e )
        {
            var position = GetButtonTag<Position>( sender );
            var window = new EditPosition( position );
            window.Closed += ( s, a ) => _context.SaveChanges();
            window.ShowDialog();
        }

        private void NewPosition( object sender, RoutedEventArgs e )
        {
            var window = new EditPosition( new Position() );
            window.Closed += ( s, a ) =>
                {
                    _context.Positions.Add( window.Position );
                    _context.SaveChanges();
                };
            window.ShowDialog();
        }

        private void NewAccount( object sender, RoutedEventArgs e )
        {
            _context.Employees.Load();
            _context.Roles.Load();
            var window = new EditAccounts(
                new Membership(),
                new ListCollectionView( _context.Employees.Local ),
                new ListCollectionView( _context.Roles.Local ) );
            window.Closed += ( s, a ) =>
            {
                Security.CreateUser( window.Account.Employee.Name, window.Account.Password, window.Account.Employee.Id, window.Role.Id );
            };
            window.ShowDialog();
        }

        private void NewEmployee( object sender, RoutedEventArgs e )
        {
            _context.Positions.Load();
            var window = new EditEmployee( new Employee(), new ListCollectionView( _context.Positions.Local ) );
            window.Closed += ( s, a ) =>
                {
                    try
                    {
                        _context.Employees.Add( window.Employee );
                        _context.SaveChanges();
                    }
                    catch ( Exception ex )
                    {
                        MessageBox.Show( ex.Message );
                    }
                };
            window.ShowDialog();
        }

        private void EditEmployee( object sender, RoutedEventArgs e )
        {
            var employee = GetButtonTag<Employee>( sender );
            _context.Positions.Load();
            var window = new EditEmployee( employee, new ListCollectionView( _context.Positions.Local ) );
            window.Closed += ( s, a ) =>
            {
                _context.SaveChanges();
            };
            window.ShowDialog();
        }

        private void DeleteEmployee( object sender, RoutedEventArgs e )
        {

            var employee = GetButtonTag<Employee>( sender );
            _context.Employees.Remove( employee );
            _context.SaveChanges();
        }

        private void AddMovieToSale( object sender, RoutedEventArgs e )
        {
            var movie = GetButtonTag<Movie>( sender );
            CurrentSale.Add( movie );
            movie.Quantity -= 1;
            FilteredMovies.Refresh();
        }

        private void FilterNameChanged( object sender, TextChangedEventArgs e )
        {
            BindFilteredMovies();
        }

        void BindFilteredMovies()
        {
            FilteredMovies.Filter = ( m ) =>
            {
                var movie = m as Movie;
                bool nameResult = true;
                bool genreResult = true;
                bool categoryResult = true;
                bool formatresult = true;

                if ( NameFilter.Text != "" )
                    nameResult = movie.Name.StartsWith( NameFilter.Text );

                if ( null != SelectedGenreFilter && SelectedGenreFilter.Name != "(None)" )
                    genreResult = movie.GenreId == SelectedGenreFilter.Id;

                if ( null != SelectedCategoryFilter && SelectedCategoryFilter.Name != "(None)" )
                    categoryResult = movie.CategoryId == SelectedCategoryFilter.Id;

                if ( null != SelectedFormatFilter && SelectedFormatFilter.Name != "(None)" )
                    formatresult = movie.FormatId == SelectedFormatFilter.Id;

                return nameResult && genreResult && categoryResult && formatresult;
            };
        }

        private void BindFilters()
        {
            var movies = new List<Movie>();
            // get filtered movies
            foreach ( var m in FilteredMovies.SourceCollection )
            {
                if ( null != FilteredMovies.Filter )
                {
                    if ( FilteredMovies.Filter( m ) )
                        movies.Add( m as Movie );
                }
                else
                    movies.Add( m as Movie );
            }

            GenreFilter.Filter = ( g ) =>
                {
                    var genre = g as Genre;
                    var intersection = movies.Intersect( genre.Movies );
                    return genre.Name == "(None)" || intersection.FirstOrDefault() != null;
                };

            CategoryFilter.Filter = ( c ) =>
                {
                    var category = c as Category;
                    var intersection = movies.Intersect( category.Movies );
                    return category.Name == "(None)" || intersection.FirstOrDefault() != null;
                };

            FormatFilter.Filter = ( f ) =>
                {
                    var format = f as Format;
                    var intersection = movies.Intersect( format.Movies );
                    return format.Name == "(None)" || intersection.FirstOrDefault() != null;
                };
        }

        private void GenreFilterChanged( object sender, SelectionChangedEventArgs e )
        {
            BindFilteredMovies();
        }

        private void AddClientToSale( object sender, RoutedEventArgs e )
        {
            var client = GetButtonTag<Customer>( sender );
            CurrentSale.CurrentCustomer = client;
        }

        private void CommitTheSale( object sender, RoutedEventArgs e )
        {
            if ( CurrentSale.CurrentCustomer != null )
            {
                foreach ( var s in CurrentSale )
                {
                    var sale = _context.Sales.Add( s );
                    sale.Customer = CurrentSale.CurrentCustomer;
                }

                _context.SaveChanges();

                var discont = CheckDiscont( CurrentSale.CurrentCustomer.Sales.Sum( s => s.Movie.SalesPrice * s.Quantity ) + CurrentSale.TotalPrice );
                if ( discont != null )
                {
                    CurrentSale.CurrentCustomer.Discont = discont;
                    CurrentSale.CurrentCustomer.DiscontId = discont.Id;
                    _context.SaveChanges();
                }
                FilteredClients.Refresh();
            }
            else
            {
                var discont = CheckDiscont( CurrentSale.TotalPrice );
                if ( discont != null )
                {
                    // open window to edit customer
                    var window = new EditCustomer( new Customer() );
                    window.Closed += ( s, a ) =>
                        {
                            if ( window.IsCancelled )
                                return;

                            window.Customer.Discont = discont;
                            _context.Customers.Add( window.Customer );
                            _context.SaveChanges();
                        };
                    window.ShowDialog();
                    foreach ( var s in CurrentSale )
                    {
                        var sale = _context.Sales.Add( s );
                        if ( !window.IsCancelled )
                            sale.Customer = window.Customer;
                    }
                    _context.SaveChanges();
                }
                else
                {
                    foreach ( var s in CurrentSale )
                        _context.Sales.Add( s );
                    _context.SaveChanges();
                }
            }

            GenerateReceipt();

            CurrentSale.Clear();
            FilteredMovies.Refresh();
            FilteredClients.Refresh();
            CurrentSale.CurrentCustomer = null;
        }

        /// <summary>
        /// Creates txt file and opens it
        /// </summary>
        void GenerateReceipt()
        {
            if ( CurrentSale == null || CurrentSale.Count == 0 )
                return;
            var sb = new StringBuilder();
            var total = default( Decimal );
            foreach ( var s in CurrentSale )
            {
                total += s.Movie.SalesPrice * s.Quantity;
                sb.AppendFormat( "{0,-10}{1}\t{2}\n{3}X{4}\n\n", s.Id, s.Movie.Name.PadRight( 30 ), String.Format( "{0:N}", s.Movie.SalesPrice * s.Quantity ), s.Quantity, String.Format( "{0:N}", s.Movie.SalesPrice ) );
            }

            if ( CurrentSale.CurrentCustomer != null && CurrentSale.CurrentCustomer.Discont != null )
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendFormat( "Клиент №{0}, Скидка - {1} %", CurrentSale.CurrentCustomer.Id, CurrentSale.CurrentCustomer.Discont.Percent );
                total = total - total * CurrentSale.CurrentCustomer.Discont.Percent / 100;
            }

            sb.AppendLine();
            sb.AppendLine();

            sb.AppendFormat( new CultureInfo( "en-US", false ), "СУММА {0:C}", total );
            var receipt = sb.ToString();
            MessageBox.Show( receipt );

        }

        Discont CheckDiscont( decimal total )
        {
            var disconts = _context.Disconts.ToArray();
            for ( var i = 0; i < disconts.Length; i++ )
            {
                if ( i == disconts.Length - 1 && total >= disconts[ i ].MinSum )
                    return disconts[ i ];

                if ( total >= disconts[ i ].MinSum && total < disconts[ i + 1 ].MinSum )
                    return disconts[ i ];
            }
            return null;
        }

        private void DecreaseMovieSaleQuantity( object sender, RoutedEventArgs e )
        {
            var sale = GetButtonTag<Sale>( sender );
            sale.Quantity--;
            sale.Movie.Quantity++;
            CurrentSale.Refresh();
            FilteredMovies.Refresh();
        }

        private void RemoveMovieSale( object sender, RoutedEventArgs e )
        {
            var sale = GetButtonTag<Sale>( sender );
            sale.Movie.Quantity += sale.Quantity;
            CurrentSale.Remove( sale );
            CurrentSale.Refresh();
            FilteredMovies.Refresh();
        }

        private void ClientNameFilterChanged( object sender, TextChangedEventArgs e )
        {
            FilteredClients.Filter = ( c ) =>
                {
                    var client = c as Customer;
                    return client.Name.StartsWith( ClientNameFilter.Text );
                };
        }



        private void ApplyReport( object sender, RoutedEventArgs e )
        {
            SalesReport.Filter = ( obj ) =>
            {
                var sale = obj as Sale;

                return sale.Date >= DateFrom.SelectedDate && sale.Date <= DateTo.SelectedDate.Value.AddDays( 1 );
            };
            SalesReport.Refresh();
            var bindingExpression = SaleaReportTotalPrice.GetBindingExpression( TextBlock.TextProperty );
            bindingExpression.UpdateTarget();
        }

        private void ResetReport( object sender, RoutedEventArgs e )
        {
            SalesReport.Filter = null;
            SalesReport.Refresh();
            var bindingExpression = SaleaReportTotalPrice.GetBindingExpression( TextBlock.TextProperty );
            bindingExpression.UpdateTarget();
        }

        private void TabControl_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if ( ReportsTab.IsSelected )
            {
                var bindingExpression = SaleaReportTotalPrice.GetBindingExpression( TextBlock.TextProperty );
                bindingExpression.UpdateTarget();
            }
        }

    }
}
