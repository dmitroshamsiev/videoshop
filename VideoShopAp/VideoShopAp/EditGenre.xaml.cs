﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditGenre.xaml
    /// </summary>
    public partial class EditGenre : Window
    {
        /// <summary>
        /// Edited genre.
        /// </summary>
        public Genre Genre { get; set; }

        /// <summary>
        /// Initializes a new instance of the EditGenre class.
        /// </summary>
        /// <param name="genre">Genre instance to edit</param>
        public EditGenre( Genre genre)
        {
            InitializeComponent();
            Genre = genre;

            DataContext = this;
        }

        /// <summary>
        /// Save the genre and closes window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveGenre( object sender, RoutedEventArgs e )
        {
            this.Close();
        }
    }
}
