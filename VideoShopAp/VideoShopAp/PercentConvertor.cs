﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace VideoShopAp
{
    class PercentConvertor
         : IValueConverter
    {
        #region IValueConverter Members

        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            return String.Format( "{0}%", value );
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var percent = value.ToString().TrimEnd( '%' );
            return Int32.Parse( percent );
        }

        #endregion
    }
}
