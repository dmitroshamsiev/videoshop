﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditPosition.xaml
    /// </summary>
    public partial class EditPosition : Window
    {
        /// <summary>
        /// Edited Position
        /// </summary>
        public Position Position { get; private set; }

        /// <summary>
        /// Initializes a new instance of the EditPosition class.
        /// </summary>
        /// <param name="position">Position to edit</param>
        public EditPosition(Position position)
        {
            InitializeComponent();
            Position = position;
            DataContext = this;
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            this.Close();
        }
    }
}
