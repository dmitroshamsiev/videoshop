﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditDiscont.xaml
    /// </summary>
    public partial class EditDiscont : Window
    {
        /// <summary>
        /// Edited discont instance
        /// </summary>
        public Discont Discont { get; set; }
        /// <summary>
        /// Initializes a new instance of the EditDiscont class.
        /// </summary>
        /// <param name="discont">Discount instance to edit</param>
        public EditDiscont(Discont discont)
        {
            InitializeComponent();
            Discont = discont;

            DataContext = this;
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            this.Close();
        }
    }
}
