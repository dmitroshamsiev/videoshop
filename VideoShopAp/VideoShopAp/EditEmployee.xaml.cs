﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditEmployee.xaml
    /// </summary>
    public partial class EditEmployee : Window
    {
        /// <summary>
        /// Edited employee.
        /// </summary>
        public Employee Employee { get; private set; }
        /// <summary>
        /// Initializes a new instance of the EditEmployee class.
        /// </summary>
        /// <param name="employee">Employee to edit</param>
        /// <param name="positions">All available positions.</param>
        public EditEmployee(Employee employee, ListCollectionView positions)
        {
            InitializeComponent();
            Employee = employee;
            Positions.ItemsSource = positions;
            DataContext = this;
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            this.Close();
        }
    }
}
