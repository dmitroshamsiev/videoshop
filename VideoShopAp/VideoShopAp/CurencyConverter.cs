﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace VideoShopAp
{
    class CurencyConverter
        : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var res = String.Format( culture, "{0:C}", value );
            return res;
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var val =  Decimal.Parse( ( string )value, System.Globalization.NumberStyles.Currency, culture );
            return val;
        }
    }
}
