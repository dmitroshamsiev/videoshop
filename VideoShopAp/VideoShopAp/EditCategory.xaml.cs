﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VideoShopAp
{
    /// <summary>
    /// Interaction logic for EditCategory.xaml
    /// </summary>
    public partial class EditCategory : Window
    {
        /// <summary>
        /// Category to edit.
        /// </summary>
        public Category Category { get; private set; }

        /// <summary>
        /// Initializes a new window of the EditCategory class.
        /// </summary>
        /// <param name="category">Category instance to edit.</param>
        public EditCategory(Category category)
        {
            InitializeComponent();
            Category = category;
            DataContext = this;
        }

        /// <summary>
        /// Saves the category and closes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveCategory( object sender, RoutedEventArgs e )
        {
            this.Close();
        }
    }
}
